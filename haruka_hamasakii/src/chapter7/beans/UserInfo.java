package chapter7.beans;//UserMessage


import java.io.Serializable;
import java.sql.Timestamp;

public class UserInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String login_id;
    private String password;
    private String name;
    private int branch;
    private int division;
    private String branchs_name;
    private String divisions_name;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLogin_id() {
		return login_id;
	}
	public void setLogin_id(String login_id) {
		this.login_id = login_id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getBranch() {
		return branch;
	}
	public void setBranch(int branch) {
		this.branch = branch;
	}
	public int getDivision() {
		return division;
	}
	public void setDivision(int division) {
		this.division = division;
	}
	public String getBranchs_name() {
		return branchs_name;
	}
	public void setBranchs_name(String branchs_name) {
		this.branchs_name = branchs_name;
	}
	public String getDivisions_name() {
		return divisions_name;
	}
	public void setDivisions_name(String divisions_name) {
		this.divisions_name = divisions_name;
	}




	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public void setCreated_date(Timestamp createdDate) {
		// TODO 自動生成されたメソッド・スタブ

	}



}
