package chapter7.service; //MessageService.java

import static chapter7.utils.CloseableUtil.*;
import static chapter7.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import chapter7.beans.UserInfo;
import chapter7.dao.UserInfoDao;

public class UserInfoService {

    public List<UserInfo> getUserInfo() {

        Connection connection = null;
        try {
            connection = getConnection();

            UserInfoDao userinfoDao = new UserInfoDao();
            List<UserInfo> ret = userinfoDao.getUserInfo(connection);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    public UserInfo getEdit(int value) {

    	 Connection connection = null;
         try {
             connection = getConnection();

             UserInfoDao userinfoDao = new UserInfoDao();
             UserInfo user = userinfoDao.getEdit(connection, value);

             commit(connection);

             return user;
         } catch (RuntimeException e) {
             rollback(connection);
             throw e;
         } catch (Error e) {
             rollback(connection);
             throw e;
         } finally {
             close(connection);
         }
     }

    public UserInfo getLogin_id(String value) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserInfoDao userinfoDao = new UserInfoDao();
            UserInfo user = userinfoDao.getLogin_id(connection, value);

            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}

