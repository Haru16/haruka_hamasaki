package chapter7.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter7.beans.Branch;
import chapter7.beans.Division;
import chapter7.beans.User;
import chapter7.beans.UserInfo;
import chapter7.service.BranchService;
import chapter7.service.DivisionsService;
import chapter7.service.UserInfoService;
import chapter7.service.UserService;

@WebServlet(urlPatterns = { "/settings" })
public class SettingServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

    	 int value = Integer.parseInt(request.getParameter("id"));
         UserInfo edit = new UserInfoService().getEdit(value);
         request.setAttribute("edit", edit);

         List<Branch> branch = new BranchService().getBranch();
     	request.setAttribute("branch", branch);

     	List<Division> divisions = new DivisionsService().getDivision();
     	request.setAttribute("divisions", divisions);

        request.getRequestDispatcher("setting.jsp").forward(request, response);
    }
    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {


        List<String> messages = new ArrayList<String>();
        List<Branch> branch = new BranchService().getBranch();
        List<Division> division = new DivisionsService().getDivision();
        HttpSession session = request.getSession();
        User editUser = getEditUser(request);

        if (isValid(request, messages) == true) {
                new UserService().update(editUser);
                response.sendRedirect("./");
        } else {
        	request.setAttribute("branch", branch);
         	request.setAttribute("divisions", division);
            session.setAttribute("errorMessages", messages);
            request.setAttribute("editUser", editUser);
            request.getRequestDispatcher("settings.jsp").forward(request, response);
        }
 }


    private User getEditUser(HttpServletRequest request)
            throws IOException, ServletException {

        User editUser = new User();
        editUser.setId(Integer.parseInt(request.getParameter("id")));
        editUser.setLogin_id(request.getParameter("login_id"));
        editUser.setPassword(request.getParameter("password"));
        editUser.setPassword(request.getParameter("password2"));
        editUser.setName(request.getParameter("name"));
        editUser.setBranch(Integer.parseInt(request.getParameter("branch")));
        editUser.setDivision(Integer.parseInt(request.getParameter("division")));
        return editUser;
    }


	private boolean isValid(HttpServletRequest request, List<String> messages) {

     String login_id = request.getParameter("login_id");
     String password = request.getParameter("password");
     String password2 = request.getParameter("password2");
     String name = request.getParameter("name");

     UserInfo edit = new UserInfoService().getLogin_id(login_id);

     if(!login_id.matches("^[a-zA-Z0-9]{6,20}$")) {
     	messages.add("ログインIDは半角英数字で6文字以上20文字以下です");
     }
     if(!(edit != null)) {
     	messages.add("そのアカウントは存在しています");
     }

     if (StringUtils.isEmpty(password) == true) {
     } else if(!password.matches("^[a-zA-Z0-9 -~]{6,20}$")) {
     	messages.add("パスワードは半角文字で6文字以上20文字以下です");
     } else if(!password2.equals(password)) {
     	messages.add("パスワードが一致していません");
     }

     if(name.length() > 10) {
     	messages.add("ユーザー名は10文字以内で入力してください");
     }
     // TODO
     if (messages.size() == 0) {
         return true;
     } else {
         return false;
}
}
}