package chapter7.service;

import static chapter7.utils.CloseableUtil.*;
import static chapter7.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import chapter7.beans.Division;
import chapter7.dao.DivisionsDao;


public class DivisionsService  {

    	public List<Division> getDivision() {

    		Connection connection = null;
    		try {
    			connection = getConnection();

    			DivisionsDao divisionDao = new DivisionsDao();
    			List<Division> ret = divisionDao.select(connection);

    			commit(connection);

    			return ret;
    		}catch (RuntimeException e) {
    			rollback(connection);
    			throw e;
    		} catch (Error e) {
    			rollback(connection);
    			throw e;
    		} finally {
    			close(connection);
    		}

    	}

    }

