package chapter7.dao;//DBから取得

import static chapter7.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import chapter7.beans.UserInfo;
import chapter7.exception.SQLRuntimeException;

public class UserInfoDao {

    public List<UserInfo> getUserInfo(Connection connection) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");                              //ここでusersのカラムをasIDに入れる
            sql.append("users.id as id, ");
            sql.append("users.login_id as login_id, ");
            sql.append("users.name as name, ");
            sql.append("branchs.brach_name as brach_name, ");
            sql.append("divisions.division_name as division_name ");
            sql.append("FROM users ");
            sql.append("INNER JOIN branchs ");
            sql.append("ON users.branch = branchs.id ");
            sql.append("INNER JOIN divisions ");
            sql.append("ON users.division = divisions.id ");

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserInfo> ret = toUserInfoList(rs);
            return ret;

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserInfo> toUserInfoList(ResultSet rs)
            throws SQLException {

        List<UserInfo> ret = new ArrayList<UserInfo>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");                         //上で入れたものを回してる
                String login_id = rs.getString("login_id");
                String name = rs.getString("name");
                String branch = rs.getString("brach_name");
                String division = rs.getString("division_name");


                UserInfo userinfo = new UserInfo();
                userinfo.setId(id);
                userinfo.setLogin_id(login_id);
                userinfo.setName(name);
                userinfo.setBranchs_name(branch);
                userinfo.setDivisions_name(division);


                ret.add(userinfo);
            }
            return ret;
        } finally {
            close(rs);
        }
    }


    public UserInfo getEdit(Connection connection, int id) {

	    PreparedStatement ps = null;
	    try {
	    	 StringBuilder sql = new StringBuilder();
	            sql.append("SELECT ");
	            sql.append("users.id as id, ");
	            sql.append("users.login_id as login_id, ");
	            sql.append("users.name as name, ");
	            sql.append("users.branch as branch, ");
	            sql.append("users.password as password, ");
	            sql.append("users.division as division,");
	            sql.append("branchs.brach_name as brach_name, ");
	            sql.append("divisions.division_name as division_name ");
	            sql.append("FROM users ");
	            sql.append("INNER JOIN branchs ");
	            sql.append("ON users.branch = branchs.id ");
	            sql.append("INNER JOIN divisions ");
	            sql.append("ON users.division = divisions.id ");
	            sql.append("WHERE users.id = ? ");

	        ps = connection.prepareStatement(sql.toString());
	        ps.setInt(1, id);

	        ResultSet rs = ps.executeQuery();
	        List<UserInfo> userList = toUserEditList(rs);

	        if (userList.isEmpty() == true) {
	            return null;
	        } else if (2 <= userList.size()) {
	            throw new IllegalStateException("2 <= userList.size()");
	        } else {
	            return userList.get(0);
	        }

	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);

	    } finally {
	        close(ps);
	    }
	}

    private List<UserInfo> toUserEditList(ResultSet rs)
            throws SQLException {

        List<UserInfo> ret = new ArrayList<UserInfo>();

        try {

            while (rs.next()) {
                int id = rs.getInt("id");
                int branch = rs.getInt("branch");
                int division = rs.getInt("division");
                String login_id = rs.getString("login_id");
                String password = rs.getString("password");
                String name = rs.getString("name");
                String branches_name = rs.getString("brach_name");
                String divisions_name = rs.getString("division_name");

                UserInfo info = new UserInfo();
                info.setId(id);
                info.setBranch(branch);
                info.setDivision(division);
                info.setLogin_id(login_id);
                info.setPassword(password);
                info.setName(name);
                info.setBranchs_name(branches_name);
                info.setDivisions_name(divisions_name);

                ret.add(info);
            }
            return ret;
        } finally {
            close(rs);
        }

    }

    public UserInfo getLogin_id(Connection connection, String login_id) {

	    PreparedStatement ps = null;
	    try {
	    	 StringBuilder sql = new StringBuilder();
	            sql.append("SELECT ");
	            sql.append("users.login_id as login_id ");
	            sql.append("FROM users ");
	            sql.append("WHERE users.login_id = ? ");

	        ps = connection.prepareStatement(sql.toString());
	        ps.setString(1, login_id);

	        ResultSet rs = ps.executeQuery();
	        List<UserInfo> userList = toUserLogin_idList(rs);

	        if (userList.isEmpty() == true) {
	            return null;

	        } else if (2 <= userList.size()) {
	            throw new IllegalStateException("2 <= userList.size()");

	        } else {
	            return userList.get(0);
	        }

	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);

	    } finally {
	        close(ps);
	    }
	}
    private List<UserInfo> toUserLogin_idList(ResultSet rs)
            throws SQLException {

        List<UserInfo> ret = new ArrayList<UserInfo>();
        try {
            while (rs.next()) {
                String login_id = rs.getString("login_id");

                UserInfo info = new UserInfo();
                info.setLogin_id(login_id);

                ret.add(info);
            }
            return ret;
        } finally {
            close(rs);
        }

    }
    }










