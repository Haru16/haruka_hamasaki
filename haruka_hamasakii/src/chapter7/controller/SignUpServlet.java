package chapter7.controller; //登録機能用

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter7.beans.Branch;
import chapter7.beans.Division;
import chapter7.beans.User;
import chapter7.service.BranchService;
import chapter7.service.DivisionsService;
import chapter7.service.UserService;


@WebServlet(urlPatterns = { "/signup" })//top.jspのsingup
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	 protected void doGet(HttpServletRequest request,
	            HttpServletResponse response) throws IOException, ServletException {

		List<Branch> branch = new BranchService().getBranch();
    	request.setAttribute("branch", branch);

    	List<Division> division = new DivisionsService().getDivision();
    	request.setAttribute("divisions", division);

		request.getRequestDispatcher("signup.jsp").forward(request, response);
    }


	@Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();
	                                                      //空リスト
		 HttpSession session = request.getSession();
	        if (isValid(request, messages) == true) {

	        	User user = new User();
	        	user.setLogin_id(request.getParameter("id"));
	        	user.setPassword(request.getParameter("password"));
	        	user.setPassword2(request.getParameter("password2"));
	            user.setName(request.getParameter("name"));
	            user.setBranch(Integer.parseInt(request.getParameter("branch")));
	            user.setDivision(Integer.parseInt(request.getParameter("division")));


	            new UserService().register(user);

	            response.sendRedirect("./");
	        } else {
	            session.setAttribute("errorMessages", messages);
	            response.sendRedirect("signup");
	        }
	}
	private boolean isValid(HttpServletRequest request, List<String> messages) {
        String login_id = request.getParameter("id");
        String password = request.getParameter("password");
        String password2 = request.getParameter("password2");
        String name = request.getParameter("name");



        if (StringUtils.isEmpty(login_id) == true) {
            messages.add("ログインIDを入力してください");
        }
        if (StringUtils.isEmpty(password) == true) {
            messages.add("パスワードを入力してください");
        }
        if(!login_id.matches("^[a-zA-z0-9]{6,20}$")) {
			messages.add("ログインIDは半角英数字6文字以上20文字以下です。");
        }

        if(!password.matches("^[!-~]{6,20}$")) {
			messages.add("パスワードは記号を含む半角英数字6文字以上20文字以下です。");
        }else if(!password2.equals(password)) {
			messages.add("パスワードが不一致です。");
		}

        if(name.length()>10) {
        	messages.add("ユーザー名は１０文字以内で入力してください。");
        }
        // TODO
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

	}


